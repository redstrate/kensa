/*
 *  SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <QWidget>

class QLabel;
class QTreeWidget;

class EnvironmentTab : public QWidget
{
    Q_OBJECT

public:
    explicit EnvironmentTab(QWidget *parent = nullptr);

    void setProcessId(long processId);

private:
    QTreeWidget *m_dataTreeWidget = nullptr;
    QLabel *m_placeholderLabel = nullptr;
};
