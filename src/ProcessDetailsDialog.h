/*
 *  KSysGuard, the KDE System Guard
 *
 *  SPDX-FileCopyrightText: 2022 Eugene Popov <popov895@ukr.net>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <QDialog>
#include <QPersistentModelIndex>

class KMessageWidget;

class GeneralTab;
class MemoryMapsTab;
class OpenFilesTab;
class EnvironmentTab;
class StringsTab;

class ProcessDetailsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProcessDetailsDialog(QWidget *parent = nullptr);

    void setProcessId(long processId);

private:
    QPersistentModelIndex m_index;

    KMessageWidget *m_warningWidget = nullptr;
    GeneralTab *m_generalTab = nullptr;
    MemoryMapsTab *m_memoryMapsTab = nullptr;
    OpenFilesTab *m_openFilesTab = nullptr;
    EnvironmentTab *m_environmentTab = nullptr;
    StringsTab *m_stringsTab = nullptr;
};
