/*
 *  KSysGuard, the KDE System Guard
 *
 *  SPDX-FileCopyrightText: 2022 Eugene Popov <popov895@ukr.net>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <QWidget>

class QTreeView;
class QLabel;
class QLineEdit;
class QSortFilterProxyModel;

class KMessageWidget;

class MemoryMapsModel;

class MemoryMapsTab : public QWidget
{
    Q_OBJECT

public:
    explicit MemoryMapsTab(QWidget *parent = nullptr);

    void setProcessId(long processId);

private Q_SLOTS:
    void refresh();

    void onProxyModelChanged();
    void onSearchEditEditingFinished();

private:
    long m_processId = 0;
    QTreeView *m_dataTreeView = nullptr;
    MemoryMapsModel *m_dataModel = nullptr;
    QSortFilterProxyModel *m_proxyModel = nullptr;

    KMessageWidget *m_errorWidget = nullptr;
    QLineEdit *m_searchEdit = nullptr;
    QLabel *m_placeholderLabel = nullptr;
};
