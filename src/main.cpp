// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ProcessDetailsDialog.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QApplication>
#include <QCommandLineParser>
#include <QInputDialog>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("kensa");

    //KAboutData::setApplicationData(about);

    QCommandLineParser parser;
    //about.setupCommandLine(&parser);
    parser.addPositionalArgument(QStringLiteral("pid"), i18n("The process ID to inspect"));
    parser.process(app);

    long pid = 0;
    if (!parser.positionalArguments().isEmpty()) {
        bool ok;
        pid = parser.positionalArguments()[0].toLong(&ok);
        if (!ok) {
            pid = 0;
        }
    }

    if (pid == 0) {
        // TODO: this should really be a process selection
        bool ok;
        int i = QInputDialog::getInt(nullptr, i18n("QInputDialog::getInt()"), i18n("Percentage:"), 25, 0, 100, 1, &ok);
        if (ok) {
            pid = i;
        }
    }

    auto dialog = new ProcessDetailsDialog();
    dialog->setProcessId(pid);
    dialog->show();

    return app.exec();
}
