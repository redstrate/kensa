/*
 *  KSysGuard, the KDE System Guard
 *
 *  SPDX-FileCopyrightText: 2022 Eugene Popov <popov895@ukr.net>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "GeneralTab.h"

#include <KFileItemActions>
#include <KFileItemListProperties>
#include <QGraphicsOpacityEffect>
#include <QHeaderView>
#include <QLabel>
#include <QLayout>
#include <QMenu>
#include <QTreeWidget>

#include <KLocalizedString>
#include <QFileInfo>
#include <QPushButton>
#include <processcore/processes.h>

GeneralTab::GeneralTab(QWidget *parent)
    : QWidget(parent)
{
    m_dataTreeWidget = new QTreeWidget;
    m_dataTreeWidget->setAlternatingRowColors(true);
    m_dataTreeWidget->setColumnCount(2);
    m_dataTreeWidget->setHeaderHidden(true);
    m_dataTreeWidget->setRootIsDecorated(false);
    m_dataTreeWidget->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    m_dataTreeWidget->header()->setStretchLastSection(true);

    QVBoxLayout *rootLayout = new QVBoxLayout;
    rootLayout->addWidget(m_dataTreeWidget);
    setLayout(rootLayout);

    auto fileActionButton = new QPushButton(i18n("File Actions"));
    fileActionButton->setIcon(QIcon::fromTheme(QStringLiteral("quickopen-file")));
    m_fileActionsMenu = new QMenu();
    fileActionButton->setMenu(m_fileActionsMenu);
    rootLayout->addWidget(fileActionButton);

    m_placeholderLabel = new QLabel;
    m_placeholderLabel->setAlignment(Qt::AlignCenter);
    m_placeholderLabel->setMargin(20);
    m_placeholderLabel->setTextInteractionFlags(Qt::NoTextInteraction);
    m_placeholderLabel->setWordWrap(true);
    m_placeholderLabel->setText(i18nc("@info:status", "No data to display"));
    // To match the size of a level 2 Heading/KTitleWidget
    QFont placeholderFont = m_placeholderLabel->font();
    placeholderFont.setPointSize(qRound(placeholderFont.pointSize() * 1.3));
    m_placeholderLabel->setFont(placeholderFont);
    // Match opacity of QML placeholder label component
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(m_placeholderLabel);
    opacityEffect->setOpacity(0.5);
    m_placeholderLabel->setGraphicsEffect(opacityEffect);

    QVBoxLayout *placeholderLayout = new QVBoxLayout;
    placeholderLayout->addWidget(m_placeholderLabel);
    m_dataTreeWidget->setLayout(placeholderLayout);
}

void GeneralTab::setProcessId(long processId)
{
    m_dataTreeWidget->clear();

    auto processes = new KSysGuard::Processes();
    processes->updateAllProcesses();
    auto process = processes->getProcess(processId);
    if (process != nullptr) {
        QList<QTreeWidgetItem *> items;
        items << new QTreeWidgetItem({i18n("Command"), process->command()});

        const QString parentName = process->parent()->name();
        items << new QTreeWidgetItem({i18n("Parent"), QStringLiteral("%1(%2)").arg(parentName).arg(process->parentPid())});

        items << new QTreeWidgetItem({i18n("User"), QString::number(process->uid())});

        QFileInfo processCwd(QStringLiteral("/proc/%1/cwd").arg(processId));
        items << new QTreeWidgetItem({i18n("Current directory"), processCwd.symLinkTarget()});

        // Get the seconds since system boot in seconds
        QFile processInfo(QStringLiteral("/proc/uptime"));
        processInfo.open(QIODevice::ReadOnly);
        const QString procStat = QString::fromLatin1(processInfo.readAll()).split(QLatin1Char(' ')).first();
        processInfo.close();
        const auto bootTimeSecs = ceil(procStat.toDouble());

        // Get the time the process has been alive in seconds
        const auto procBootTimeSecs = process->startTime() / sysconf(_SC_CLK_TCK);

        auto dateTime = QDateTime::currentDateTime();
        dateTime = dateTime.addSecs(procBootTimeSecs - bootTimeSecs);

        items << new QTreeWidgetItem({i18n("Started"), dateTime.toString()});

        m_dataTreeWidget->addTopLevelItems(items);

        m_placeholderLabel->hide();

        QFileInfo processExe(QStringLiteral("/proc/%1/exe").arg(processId));
        KFileItemList itemList({KFileItem(QUrl::fromLocalFile(processExe.symLinkTarget()))});

        auto menuActions = new KFileItemActions(m_fileActionsMenu);
        menuActions->setItemListProperties(KFileItemListProperties(itemList));
        menuActions->setParentWidget(m_fileActionsMenu);
        menuActions->insertOpenWithActionsTo(nullptr, m_fileActionsMenu, {});
        menuActions->addActionsTo(m_fileActionsMenu);
    }
}
