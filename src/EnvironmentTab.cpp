/*
 *  SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "EnvironmentTab.h"

#include <QFile>
#include <QGraphicsOpacityEffect>
#include <QHeaderView>
#include <QLabel>
#include <QLayout>
#include <QTreeWidget>

#include <KLocalizedString>
#include <QIODeviceBase>
#include <processcore/processes.h>

EnvironmentTab::EnvironmentTab(QWidget *parent)
    : QWidget(parent)
{
    m_dataTreeWidget = new QTreeWidget;
    m_dataTreeWidget->setAlternatingRowColors(true);
    m_dataTreeWidget->setColumnCount(2);
    m_dataTreeWidget->setRootIsDecorated(false);
    m_dataTreeWidget->setHeaderLabels({i18n("Variable"), i18n("Value")});
    m_dataTreeWidget->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    m_dataTreeWidget->header()->setStretchLastSection(true);

    QVBoxLayout *rootLayout = new QVBoxLayout;
    rootLayout->addWidget(m_dataTreeWidget);
    setLayout(rootLayout);

    m_placeholderLabel = new QLabel;
    m_placeholderLabel->setAlignment(Qt::AlignCenter);
    m_placeholderLabel->setMargin(20);
    m_placeholderLabel->setTextInteractionFlags(Qt::NoTextInteraction);
    m_placeholderLabel->setWordWrap(true);
    m_placeholderLabel->setText(i18nc("@info:status", "No data to display"));
    // To match the size of a level 2 Heading/KTitleWidget
    QFont placeholderFont = m_placeholderLabel->font();
    placeholderFont.setPointSize(qRound(placeholderFont.pointSize() * 1.3));
    m_placeholderLabel->setFont(placeholderFont);
    // Match opacity of QML placeholder label component
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(m_placeholderLabel);
    opacityEffect->setOpacity(0.5);
    m_placeholderLabel->setGraphicsEffect(opacityEffect);

    QVBoxLayout *placeholderLayout = new QVBoxLayout;
    placeholderLayout->addWidget(m_placeholderLabel);
    m_dataTreeWidget->setLayout(placeholderLayout);
}

void EnvironmentTab::setProcessId(long processId)
{
    m_dataTreeWidget->clear();

    QList<QTreeWidgetItem *> items;

    const QString filePath = QStringLiteral("/proc/%1/environ").arg(processId);
    QFile file(filePath);
    if (file.open(QFile::ReadOnly)) {
        QTextStream textStream(file.readAll());
        QString line;
        while (textStream.readLineInto(&line)) {
            // FIXME: this breaks on BASH_FUNC
            const QStringList environmentVariables = line.split(QLatin1Char('\0'));
            for (const QString &variable : environmentVariables) {
                if (!variable.isEmpty()) {
                    const QStringList pair = variable.split(QLatin1Char('='));
                    if (pair.size() == 2) {
                        items << new QTreeWidgetItem({pair[0], pair[1]});
                    } else if (pair.size() == 1) {
                        items << new QTreeWidgetItem({pair[0]});
                    }
                }
            }
        }
    }

    if (!items.isEmpty()) {
        m_dataTreeWidget->addTopLevelItems(items);
        m_placeholderLabel->hide();
    }
}
