/*
 *  KSysGuard, the KDE System Guard
 *
 *  SPDX-FileCopyrightText: 2022 Eugene Popov <popov895@ukr.net>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "ProcessDetailsDialog.h"

#include <QLayout>
#include <QTabWidget>

#include <KLocalizedString>
#include <KMessageWidget>

#include <processcore/process.h>

#include "EnvironmentTab.h"
#include "GeneralTab.h"
#include "MemoryMapsTab.h"
#include "OpenFilesTab.h"
#include "StringsTab.h"

ProcessDetailsDialog::ProcessDetailsDialog(QWidget *parent)
    : QDialog(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle(i18nc("@title:window", "Kensa"));
    resize(900, 600);

    m_warningWidget = new KMessageWidget;
    m_warningWidget->setMessageType(KMessageWidget::Warning);
    m_warningWidget->setCloseButtonVisible(false);
    m_warningWidget->setWordWrap(true);
    m_warningWidget->hide();

    QTabWidget *tabWidget = new QTabWidget;

    m_generalTab = new GeneralTab;
    tabWidget->addTab(m_generalTab, i18nc("@title:tab", "General"));

    m_memoryMapsTab = new MemoryMapsTab;
    tabWidget->addTab(m_memoryMapsTab, i18nc("@title:tab", "Memory Maps"));

    m_openFilesTab = new OpenFilesTab;
    tabWidget->addTab(m_openFilesTab, i18nc("@title:tab", "Open Files"));

    m_environmentTab = new EnvironmentTab;
    tabWidget->addTab(m_environmentTab, i18nc("@title:tab", "Environment"));

    m_stringsTab = new StringsTab;
    tabWidget->addTab(m_stringsTab, i18nc("@title:tab", "Strings"));

    QVBoxLayout *rootLayout = new QVBoxLayout;
    rootLayout->addWidget(m_warningWidget);
    rootLayout->addWidget(tabWidget);
    setLayout(rootLayout);
}

void ProcessDetailsDialog::setProcessId(long processId)
{
    m_generalTab->setProcessId(processId);
    m_memoryMapsTab->setProcessId(processId);
    m_openFilesTab->setProcessId(processId);
    m_environmentTab->setProcessId(processId);
    m_stringsTab->setProcessId(processId);
}
