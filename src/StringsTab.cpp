/*
 *  SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "StringsTab.h"

#include <QFile>
#include <QGraphicsOpacityEffect>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QListView>
#include <QTimer>

#include <KLocalizedString>
#include <QIODeviceBase>
#include <processcore/processes.h>

StringsTab::StringsTab(QWidget *parent)
    : QWidget(parent)
{
    m_searchEdit = new QLineEdit;
    m_searchEdit->setPlaceholderText(i18n("Search…"));

    m_dataTreeWidget = new QListView;
    m_dataTreeWidget->setAlternatingRowColors(true);

    m_dataModel = new QStringListModel();

    m_proxyModel = new QSortFilterProxyModel(this);
    m_proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setFilterKeyColumn(-1);
    m_proxyModel->setSortRole(Qt::EditRole);
    m_proxyModel->setSourceModel(m_dataModel);
    m_dataTreeWidget->setModel(m_proxyModel);

    QVBoxLayout *rootLayout = new QVBoxLayout;
    rootLayout->addWidget(m_searchEdit);
    rootLayout->addWidget(m_dataTreeWidget);
    setLayout(rootLayout);

    // use some delay while searching as you type, because an immediate
    // search in large data can slow down the UI
    QTimer *applySearchTimer = new QTimer(this);
    applySearchTimer->setInterval(350);
    applySearchTimer->setSingleShot(true);

    connect(m_searchEdit, &QLineEdit::textChanged, applySearchTimer, qOverload<>(&QTimer::start));
    connect(m_searchEdit, &QLineEdit::editingFinished, applySearchTimer, &QTimer::stop);
    connect(m_searchEdit, &QLineEdit::editingFinished, this, &StringsTab::onSearchEditEditingFinished);

    connect(applySearchTimer, &QTimer::timeout, this, &StringsTab::onSearchEditEditingFinished);

    m_placeholderLabel = new QLabel;
    m_placeholderLabel->setAlignment(Qt::AlignCenter);
    m_placeholderLabel->setMargin(20);
    m_placeholderLabel->setTextInteractionFlags(Qt::NoTextInteraction);
    m_placeholderLabel->setWordWrap(true);
    m_placeholderLabel->setText(i18nc("@info:status", "No data to display"));
    // To match the size of a level 2 Heading/KTitleWidget
    QFont placeholderFont = m_placeholderLabel->font();
    placeholderFont.setPointSize(qRound(placeholderFont.pointSize() * 1.3));
    m_placeholderLabel->setFont(placeholderFont);
    // Match opacity of QML placeholder label component
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(m_placeholderLabel);
    opacityEffect->setOpacity(0.5);
    m_placeholderLabel->setGraphicsEffect(opacityEffect);

    QVBoxLayout *placeholderLayout = new QVBoxLayout;
    placeholderLayout->addWidget(m_placeholderLabel);
    m_dataTreeWidget->setLayout(placeholderLayout);
}

void StringsTab::setProcessId(long processId)
{
    m_dataModel->setStringList({});

    QList<QString> items;

    const QString filePath = QStringLiteral("/proc/%1/exe").arg(processId);
    QFile file(filePath);
    if (file.open(QFile::ReadOnly)) {
        QTextStream textStream(file.readAll());
        QString line;
        while (textStream.readLineInto(&line)) {
            QString finalString;
            int numGraphicChars = 0;
            for (const auto &character : line) {
                if (character != QChar::ReplacementCharacter) {
                    finalString.append(character);
                    numGraphicChars++;
                } else {
                    numGraphicChars = 0;
                }
            }
            if (numGraphicChars >= 4) {
                items << finalString;
            }
        }
    }

    if (!items.isEmpty()) {
        m_dataModel->setStringList(items);
        m_placeholderLabel->hide();
    }
}

void StringsTab::onSearchEditEditingFinished()
{
    m_proxyModel->setFilterFixedString(m_searchEdit->text());
}