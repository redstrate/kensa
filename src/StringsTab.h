/*
 *  SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <QSortFilterProxyModel>
#include <QStringListModel>
#include <QWidget>

class QLabel;
class QListView;
class QLineEdit;

class StringsTab : public QWidget
{
    Q_OBJECT

public:
    explicit StringsTab(QWidget *parent = nullptr);

    void setProcessId(long processId);

private Q_SLOTS:
    void onSearchEditEditingFinished();

private:
    QListView *m_dataTreeWidget = nullptr;
    QLabel *m_placeholderLabel = nullptr;
    QSortFilterProxyModel *m_proxyModel = nullptr;
    QStringListModel *m_dataModel = nullptr;
    QLineEdit *m_searchEdit = nullptr;
};
