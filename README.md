# Kensa

View detailed information about a process.

## Features

* View general information like the parent process & PID, the commandline and the current directory of a process.
* Check the memory mapped by the process. For example, the shared libraries it has loaded.
* Show the open files and sockets of the process.
* View the environment variables the process was launched with.

Note that Kensa is **not**:
* A multi-process viewer or system monitor, use [Plasma System Monitor](https://invent.kde.org/plasma/plasma-systemmonitor). Kensa is focused around viewing information on a single process.
* Designed for Windows or macOS systems. Patches are appreciated for that, but I don't use Kensa there so don't expect lots of support.

## Get It

Builds are available for Fedora users through
my [personal COPR](https://copr.fedorainfracloud.org/coprs/redstrate/personal/):

```shell
sudo dnf copr enable redstrate/personal 
sudo dnf install kensa
```

## Credits

[libksysguard](https://invent.kde.org/plasma/libksysguard) where most of the original code is from. 